package noobbot;

public class InformazioniGaraJSON {
    public String msgType;
    public Object data;
    public Race race;
	
    InformazioniGaraJSON(final String msgType, final Object data, final Race race) {
    	
    	// Parsare tutto il contenuto di "data" per completare l'oggetto InformazioniGaraJSON
        this.msgType = msgType;
        this.data = data;
        this.race = race;
    }
    
    public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Race getRace() {
		return race;
	}

	public void setRace(Race race) {
		this.race = race;
	}
}
