package noobbot;

public class RaceSession {
	public final int laps;
	public final int maxLapTimeMs;
	public final boolean quickRace;
	
	RaceSession(final int laps, final int maxLapTimeMs, final boolean quickRace) {
		this.laps = laps;
		this.maxLapTimeMs = maxLapTimeMs;
		this.quickRace = quickRace;
	}
}