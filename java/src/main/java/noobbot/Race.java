package noobbot;

public class Race {
	public final Track track;
	public final Cars cars;
	public final RaceSession raceSession;
	
	Race(final Track track, final Cars cars, final RaceSession raceSession) {
		this.track = track;
		this.cars = cars;
		this.raceSession = raceSession;
	}
	
}