package noobbot;

public class Id {
	public final String name;
	public final String color;
	
	Id(final String name, final String color) {
		this.name = name;
		this.color = color;
	}
}