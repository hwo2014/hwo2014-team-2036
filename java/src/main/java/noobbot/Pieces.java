package noobbot;

public class Pieces {
	private String pieceType;
	private double length;
	private double radius;
	private double angle;
	private boolean switchLane;
	
	public String getPieceType() {
		return pieceType;
	}
	public void setPieceType(String pieceType) {
		this.pieceType = pieceType;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle = angle;
	}
	public boolean isSwitchLane() {
		return switchLane;
	}
	public void setSwitchLane(boolean switchLane) {
		this.switchLane = switchLane;
	}
}