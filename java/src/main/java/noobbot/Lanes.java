package noobbot;

public class Lanes {
	private double distanceFromCenter;
	private int index;
	
	public double getDistanceFromCenter() {
		return distanceFromCenter;
	}
	public void setDistanceFromCenter(double distanceFromCenter) {
		this.distanceFromCenter = distanceFromCenter;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
}