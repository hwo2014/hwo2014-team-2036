package noobbot;

public class Cars {
	public final Id id;
	public final int dimensions;
	
	Cars(final Id id, final int dimensions) {
		this.id = id;
		this.dimensions = dimensions;
	}
}