package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);
        
        int cont = 0;
        
        InformazioniGaraJSON informazioniPartita;

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                //send(new Throttle(0.5));
            	
                
                
            	cont++;
            	
            	if(cont == 1)
            		send(new Throttle(0.65));
            	else
            		send(new SwitchLane("Right"));
            	
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	
            	
//            	Gson gson = new Gson();
            	informazioniPartita = gson.fromJson(line, InformazioniGaraJSON.class);
            	informazioniPartita.getMsgType();
            	informazioniPartita.getRace();
            	
//            	parsare il json con "line" usando indexat e spostandosi negli elementi.
//            	LettoreJSON lettJSON = gson.fromJson (line, LettoreJSON.class);
                
//            	JsonObject obj = (JsonObject)lett.data;
            	
//            	System.out.println(obj.getAsString());
            	
            	// Salvare le informazioni dei tracciati
            	
//            	LettoreJSON objTracciato = gson.fromJson(reader, LettoreJSON.class);
             
//            	System.out.println(objTracciato);
//            	
//            	
//            	System.out.println(objTracciato.data.race.track.getName());
//            	System.out.println(objTracciato.data.race.track.getId());
//            	
//            	tracciati = msgFromServer.data.toString();
            	
            	
//            	tracciato = new Tracciato();
//            	tracciato.set
            	
//            	System.out.println(tracciati);
            	
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;
    
    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class LettoreJSON extends MsgWrapper {
//	public final String msgType;
    
    public final String gameId;
    
    LettoreJSON(final String msgType, final Data data, final String gameId) {
    	super(msgType, data);
//        this.msgType = msgType;
        this.gameId = gameId;
    }
}