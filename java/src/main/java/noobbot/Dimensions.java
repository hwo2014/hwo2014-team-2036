package noobbot;

public class Dimensions {
	public final double length;
	public final double width;
	public final double guideFlagPosition;
	
	Dimensions(final double length, final double width, final double guideFlagPosition) {
		this.length = length;
		this.width = width;
		this.guideFlagPosition = guideFlagPosition;
	}
}